package anthony.muller.playground.learningsb2;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import reactor.core.publisher.Flux;

@Configuration
public class LoadDatabase {

	@Bean
	CommandLineRunner init(ChapterRepository repository) {
		return args -> {
			Flux.just(
				new Chapter("1st chapter"),
				new Chapter("2nd chapter"),
				new Chapter("3rd chapter")
			)
			.flatMap(repository::save)
			.subscribe(System.out::println);
		};
	}
}
