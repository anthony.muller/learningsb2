package anthony.muller.playground.learningsb2;

import java.util.function.Supplier;

import org.junit.Test;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * http://bit.ly/reactive-part-1
 * http://bit.ly/reactive-part-2
 * http://bit.ly/reactive-part-3
 * http://bit.ly/reactive-types
 */
public class ReactiveProgrammingTests {

	@Test
	public void basicFlux() {
		Flux.just("alpha", "bravo", "charlie")
		//.map((s) -> {return s;})
		.subscribe(System.out::println);
	}
	
	@Test
	public void fluxWithSupplier() {
		Flux.just(
			(Supplier<String>) () -> "alpha",
			(Supplier<String>) () -> "bravo",
			(Supplier<String>) () -> "charlie"
		)
		// .subscribe(System.out::println);
		.subscribe( (supplier) -> System.out.println(supplier.get()));
	}
	
	@Test
	public void combinedOperations() {
		Flux.just("alpha", "bravo", "charlie")
		.map(String::toUpperCase)
		.flatMap(s -> Flux.fromArray(s.split("")))
		.groupBy(String::toString)
		.sort((o1, o2) -> o1.key().compareTo(o2.key()))
		.flatMap(group -> Mono.just(group.key()).zipWith(group.count()))
		.map(keyAndCount -> keyAndCount.getT1() + " => " + keyAndCount.getT2())
		.subscribe(System.out::println);
	}
}
